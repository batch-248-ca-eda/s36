// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

// this allows us to use all the routes defined in taskRoutes.js
const taskRoutes = require("./routes/taskRoutes.js")

// Setup Server
const app = express();
const port = 4000;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database Connection
// Connecting to MongoDB Atlas

mongoose.set('strictQuery',true);

mongoose.connect("mongodb+srv://admin:admin123@clusterbatch248.5uy7vj2.mongodb.net/s35?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Connection to database
let db = mongoose.connection;

db.on("error",console.error.bind(console,"connection error"));
db.once("open",()=>console.log("Hi! We are connected to MongoDB Atlas!"))

// Add the task route
// Allows all the task routes created in the taskRoutes.js file to use "/tasks" route
app.use("/tasks", taskRoutes)

// Server listening
app.listen(port,()=>console.log(`Now listening to port ${port}`))